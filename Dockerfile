FROM ubuntu:latest
LABEL version="1"
LABEL description="appweb para Infraestructura de Servidores"
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install nginx -y 

#Vamos a utilizar la instruccion ADD para copiar los ficheros desde Gitlab


ADD https://gitlab.com/gonzalo.buontempo/calculadora_ids/-/raw/main/index.html /var/www/html
ADD https://gitlab.com/gonzalo.buontempo/calculadora_ids/-/raw/main/estiloscalculadora.css /var/www/html
#Agregamos permisos al directorio 
RUN chmod +r /var/www/* -R

ADD https://gitlab.com/gonzalo.buontempo/calculadora_ids/-/raw/main/default /etc/nginx/sites-available


ADD https://gitlab.com/gonzalo.buontempo/calculadora_ids/-/raw/main/entrypoint.sh /

# Agregamos permisos para poder ejecutar el script entrypoint.sh

RUN chmod +x /entrypoint.sh


EXPOSE 80 443


# La instruccion 'ENTRYPOINT' indica que script se ejecutara cuando se lanze el contenedor
ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
