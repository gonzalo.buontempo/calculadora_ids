#!/bin/bash
# si el script produce algun error, que se detenga su ejecucion
set -e

# Paso la variable de entorno que esta en el archivo index

sed -i 's/SITE_NAME/'"${SITE_NAME}"'/' /var/www/html/index.html

sed -i 's/Materia/'"${Materia}"'/' /var/www/html/index.html

# '$@' representa arugmentos pasados al script, estos argumentos son otros scripts.
exec "$@"
