# Final Infraestructura de Servidores

**Materia: Infraestructura de Servidores.**

**Nombre y apellido: Gonzalo Nicolas Buontempo**

**Profesor: Sergio Pernas**

**Fecha: 12/7/2022**

[TOC]



### Implementación de  Docker 

En este proyecto crearemos un imagen Docker, un Dockerfile y utilizaremos un repositorio de GitLab para dejar los archivos necesarios para crear nuestra imagen docker y poder realizar el deploy.


### <u>REQUISITOS:</u>

<u>**Requisitos de Hardware: **</u>

- 2 CPU 

- 2 GB de RAM

- 20 GB de disco para el SO

  

  <u>Para realizar este procedimiento debemos tener:</u>

- Una maquina virtual con Ubuntu 18 instalado mínimo.

- Tener instalado docker.

- Un hostname asignado al servidor Ubuntu.

- Tener acceso a internet para poder descargar los paquetes necesarios. 

- Tener una cuenta en Git Lab.
  

### <u>INTRUCCIONES</u>

#### 1- Instalación y configuracion de Docker.

El primer paso será instalar los  paquetes desde el repositorio oficial de docker.

- Actualizamos e instamos los paquetes y dependencias necesarias.

```
 sudo apt update && sudo install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```

- Importamos las llaves 

  ``` 
  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
  ```

  Agregamos repositorio

  ``` 
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  ```
  
  Hacemos un update

  ``` 
  apt update
  ```

- Instalamos Docker

  ``` 
   apt install docker-ce docker-ce-cli containerd.io
  ```

- Listamos los contenedores

  ``` 
  docker ps
  CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
  ```

  'COMMAND'  El programa que se ejecuta cuando se lanza un contenedor.

  'Status' El estado del comando que se ejecuta cuando se lanza el contenedor.

  Exited (0) Significa que el programa termino correctamente si nos devuelve un numero distinto de 0 significa que el programa tiene algun error.

  

#### 2- Repositorio Git Lab

En primera instancia creamos una cuenta en Gitlab.

Luego debemos crear las llaves para poder utilizar nuestro repositorio desde nuestra VM con Ubuntu.

Para ello debemos correr el siguiente comando. (Luego de la variable -C, entre comillas irá la cuenta generada en Gitlab, en mi caso es gonzalo.buontempo@istea.com.ar)

```
ssh-keygen -t rsa -b 2048 -C "gonzalo.buontempo@istea.com.ar"
```

Esto nos va a crear nuestras llaves ssh las cuales debemos asociarlas a nuestro reposito de Gitlab en preferencias --> Claves SSH

Lo proximo que debemos haces es crear nuestro proyecto y volcar nuestro DockerFile, scripts y demás archivos de configuración.

#### 3- Explicación de archivos de configuración

Un Dockerfile es un script que contiene los comandos necesarios para crear nuestra imagen docker. Debemos pasarle todas las instrucciones necesarias que iran construyendo nuestra imagen. 

El fichero Dockerfile tiene configurados varios archivos que se descargarán desde el repositorio de GitLab y se guardarán en una ubicacion especifica para crear la imagen docker. 

**Dockerfile**

**Con la instruccion FROM le decimos de donde queremos tomar la imagen base para crear nuestra imagen docker.**

`FROM ubuntu:latest`

**Label son etiquetas, en este caso se definieron la etiqueta de versionado y la descripcion breve del proyecto.**

`LABEL version="1"`
`LABEL description="appweb para Infraestructura de Servidores"`

**La instruccion ARG setea variables de entorno dentro del contenedor para crear la imagen. En este caso se definio que el despliegue no sea interactivo**

`ARG DEBIAN_FRONTEND=noninteractive`

**Con RUN se definen comandos a correr. En este caso primero se hará un update y luego se instalará nginx.**

`RUN apt update && apt install nginx -y`

**Utilizamos la instruccion ADD para copiar los ficheros que tenemos en el repositorio de GitLab.**

**Copiamos el archivo index.html y el archivo al directorio /var/www/html**

`ADD https://gitlab.com/gonzalo.buontempo/calculadora_ids/-/raw/main/index.html /var/www/html`

`ADD https://gitlab.com/gonzalo.buontempo/calculadora_ids/-/raw/main/estiloscalculadora.css /var/www/html`

**Agregamos permisos al directorio de manera recursiva.**
`RUN chmod +r /var/www/* -R`

**Copiamos desde gitlab el archivo default al directorio /etc/nginx/sites-available** 

`ADD https://gitlab.com/gonzalo.buontempo/calculadora_ids/-/raw/main/default /etc/nginx/sites-available`

**Copiamos desde gitlab el script entrypoint.sh al directorio raiz /** 

`ADD https://gitlab.com/gonzalo.buontempo/calculadora_ids/-/raw/main/entrypoint.sh /`

**Agregamos permisos para poder ejecutar el script entrypoint.sh**

`RUN chmod +x /entrypoint.sh`

**Exponemos los puertos 80 y 443 del contenedor**

`EXPOSE 80 443`

**La instrucción ENTRYPOINT indica que script se ejecutara cuando se lanze el contenedor**

`ENTRYPOINT ["/entrypoint.sh"]`

**La instrucción CMD indica que comando se ejecutará cuando el contenedor se inicie.**

`CMD ["nginx", "-g", "daemon off;"]`



**Archivo default**

**Este archivo valida lo que entra por el puerto 80 hacia el servidor y muestra el archivo index.html que se encuentra en el directorio /var/www/html este directorio es el default de la aplicacion web**

```
server {
 listen 80 default_server;
 root /var/www/html;
 index index.html index.htm index.nginx-debian.html;
 server_name _;
 location / {
 try_files $uri $uri/ =404;
 }
}

```



**Archivo index.html**

**En el archivo index.html se configura la pagina Web, de la misma vamos a utilizar 2 variables que luego se definen en el script entrypoint.sh**

```
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" /><!-- Etiqueta para adaptar a @media -->
  <LINK REL="stylesheet" TYPE="text/css" HREF="estiloscalculadora.css"><!-- Enlace hoja estilos -->
</head>
<body>
<!-- Empieza codigo calculadora html-->
  <form name="calculator">
    <h1 style="padding-left: 40px;"><strong>SITE_NAME</strong></h1>
	<h3>Calculadora para Materia</h3>
    <p>Haz algun calculo para ver el resultado.</p>
    <input type="textfield" name="ans" value="">
    <br>
    <input type="button" value="1" onClick="document.calculator.ans.value+='1'">
    <input type="button" value="2" onClick="document.calculator.ans.value+='2'">
    <input type="button" value="3" onClick="document.calculator.ans.value+='3'">
    <input type="button" value="+" onClick="document.calculator.ans.value+='+'">
    <br>
    <input type="button" value="4" onClick="document.calculator.ans.value+='4'">
    <input type="button" value="5" onClick="document.calculator.ans.value+='5'">
    <input type="button" value="6" onClick="document.calculator.ans.value+='6'">
    <input type="button" value="-" onClick="document.calculator.ans.value+='-'">
    <br>
    <input type="button" value="7" onClick="document.calculator.ans.value+='7'">
    <input type="button" value="8" onClick="document.calculator.ans.value+='8'">
    <input type="button" value="9" onClick="document.calculator.ans.value+='9'">
    <input type="button" value="*" onClick="document.calculator.ans.value+='*'">
    <br>
    <input type="button" value="0" onClick="document.calculator.ans.value+='0'">
    <input type="reset" value="c">
    <input type="button" value="/" onClick="document.calculator.ans.value+='/'">
    <input type="button" value="=" onClick="document.calculator.ans.value=eval(document.calculator.ans.value)">
    </form>
</body>
</html>
<!-- Finaliza codigo calculadora html -->
```

**Archivo estiloscalculadora.css**

**En el archivo estiloscalculadora.css se encunetran las configuraciones de diseño del sitio web**

```
form {
  width: 100%;
  max-width: 400px;
  text-align: center;
  border: solid 1px #c2c2c2;
  padding-bottom: 10px;
  margin: auto;
  background: #40CFFF;
}
input[type=textfield] {
    width: 75%;
    padding: 16px 32px;
    font-size: 16px;
    margin: 8px 0;
    border: 1px solid silver;
    border-radius: 1px;
    text-align: left;
    color: #333;
    background: #ccc;
}
input[type=button], input[type=submit], input[type=reset] {
    background-color: #0000FF;
    border: none;
    color: white;
    padding: 16px 32px;
    font-size: 16px;
    min-width: 21%;
    text-decoration: none;
    margin: 4px 2px;
    cursor: pointer;
}
input[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover {
  background-color: #333;
}
```

**Archivo entrypoint.sh**

**Para comenzar usamos los comando #!/bin/bash ya que estamos usando un script con bash**

`#!/bin/bash`

**El comando set-e se va a detener el script  si ve un error en la ejecucion**

`set -e`

**Enviamos las variables para reemplazar en el archivo index.html**

`sed -i 's/SITE_NAME/'"${SITE_NAME}"'/' /var/www/html/index.html`

`sed -i 's/Materia/'"${Materia}"'/' /var/www/html/index.html`

**El "$@" representa argumentos pasados al script, estos argumentos son los otros scripts.**

`exec "$@"`


#### 4- Crear imagen Docker

Ahora vamos a crear nuestra imagen docker con todos los files explicados anteriormente. 

* Ahora vamos a entrar al directorio 

  `cd /etc/docker/`

* Una vez dentro descargamos todo el contenido de gitlab.

  `git clone https://gitlab.com/gonzalo.buontempo/calculadora_ids.git `

* Esto nos crea la carpeta calculadora_ids con todos los ficheros del repositorio

```
gonza@ubuntu:/etc/docker/calculadora_ids$ ls -la
total 40
drwxr-xr-x 3 root root 4096 jul 11 01:52 .
drwxr-xr-x 3 root root 4096 jul 11 01:52 ..
-rw-r--r-- 1 root root  176 jul 11 01:52 default
-rw-r--r-- 1 root root  972 jul 11 01:52 Dockerfile
-rw-r--r-- 1 root root  364 jul 11 01:52 entrypoint.sh
-rw-r--r-- 1 root root  748 jul 11 01:52 estiloscalculadora.css
drwxr-xr-x 8 root root 4096 jul 11 01:52 .git
-rw-r--r-- 1 root root 1977 jul 11 01:52 index.html
-rw-r--r-- 1 root root 6288 jul 11 01:52 README.md

```

* Ahora vamos a crear nuestra imagen docker con el siguiente comando va a compilar la imagen

```
sudo docker build -t calculadora_ids .
```



#### 5- Deploy de Docker con nuestra imagen creada.

- Ahora con la imagen anteriormente creada vamos a realizar el deploy de la imagen.

  * Debemos tener en cuenta los siguiente comandos  `-d` corre en modo deamon. 
  *  `-p` especificamos los puertos locales y del contenedor   
  * `-e` le pasamos todas las variables que queremos remplazar del index.html
  * `--name` luego de este comando especificamos el nombre del contenedor 
  * por ultimo ponemos el nombre de la imagen que creamos `calculadora_ids`

  ````
  sudo docker run -d -p 8081:80 -e SITE_NAME="Calculadora Web" -e Materia="Infraestructura de servidores" --name calculadora_ids calculadora_ids
  ````

  

  * Verificamos si el contenedor corrio con exito usando el comando `docker ps -a`

```
CONTAINER ID   IMAGE     		 COMMAND                  CREATED          STATUS          			 PORTS                                        	  NAMES
439c2966f4bf   calculadora_ids   "/entrypoint.sh ngin…"   42 seconds ago   Up 41 seconds             443/tcp, 0.0.0.0:8081->80/tcp, :::8081->80/tcp   calculadora_ids  
```

* Verificamos por http entrar a la ip de nuestro server y verificamos la pagina 
